#!/usr/bin/python
import sys
import operator
import csv

def calculate_appearences(full_path):
    elements = {}
    with open(full_path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            elements = get_elems(row[1], elements)
        write_ouput(elements, full_path)

def get_elems(row,elements):
    row_items = get_elem(row)
    for elem_item in row_items:
        if (elem_item in elements) and elem_item:
            elements[elem_item] += 1
        elif elem_item:
            elements[elem_item] = 1
    return elements

def get_elem(row):
    q_string = row
    regex = "%%"
    row_elems = []
    while q_string.find(regex) >= 0:
        string = q_string[q_string.find(regex)+2:]
        row_elems.append(string[:string.find(regex)])
        q_string = substring_to_next(string, regex)
    return row_elems

def substring_to_next(elem_string, regex):
    string = elem_string[elem_string.find(regex):]
    string = string[string.find(regex)+len(regex):]
    string = string[string.find(regex):]
    return string


def write_ouput(dic, full_path):
    output_path = full_path[:full_path.find(".csv")]+"-report.csv"
    tuples = sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    with open(output_path, 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar=',', quoting=csv.QUOTE_MINIMAL)
        for x, y in tuples:
            spamwriter.writerow([x, y])
    print "New file has been created "+output_path

### Main ###
## Example line to parse:  %%networkspecificmediumutm%%something%%veryimportan%%trashandmoretrash%%moreinformation%%
if len(sys.argv) > 1:
    full_path = sys.argv[1]
    print full_path
    calculate_appearences(full_path)
else:
    print "Path is required."


